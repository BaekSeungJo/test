package com.plnc.lostless.core.action;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothGatt;
import android.bluetooth.BluetoothGattCallback;
import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothGattService;
import android.bluetooth.BluetoothManager;
import android.bluetooth.BluetoothProfile;
import android.content.Context;
import com.plnc.lostless.core.manager.OutOfRangeManager;
import com.plnc.lostless.core.model.ActionResult;
import java.lang.reflect.Method;
import java.util.List;
import timber.log.Timber;

// merge 테스트 by tutorial2
// git 테스트
// merge 테스트
/**
 * Created by User on 2015-05-28.
 * Commented by yhlee on 2015-09-01
 *     bluetooth에 대한 어떠한 액션들이 정의 된 곳이다.(상위 class 정의)
 *     예를 들어 비콘을 unlock하기 위해선 Bluetoothconnect, discover service, discover characteristics, wirte characteristics의 과정을 모두 거쳐야 unlock이 되는데
 *     그러한 액션들을 중복 코드를 최대한 제거하여(BaseAction을 이용)수행하도록 구현되어 있다.
 *     하지만 현재는 비콘과 action을 통해서 수행할 것은 BeaconUnlockAction만 존재한다.(나머지는 현 버전에서는 사용하지 않는다.)
 *     BaseAction은 abstarct class로 실제로 new로 생성될 수는 없고 상속하여 사용할 수 있다.
 */
public abstract class BaseAction extends BluetoothGattCallback {
    /**
     * ActionResult의 status값으로 beacon command실행 후 정상
     */
    public final static boolean SUCCESS = true;
    /**
     * ActionResult의 status값으로 beacon command실행 후 실패
     */
    public final static boolean FAIL = false;

    protected Context context;

    protected BluetoothManager mBluetoothManager;
    protected BluetoothAdapter mBluetoothAdapter;
    protected BluetoothGatt mBluetoothGatt;
    protected BluetoothDevice mBluetoothDevice;

    protected List<BluetoothGattService> mBluetoothGattServiceList;
    protected List<BluetoothGattCharacteristic> mBluetoothGattCharacteristicList;

    protected String macAddress;

    protected int mState = STATE_NONE;
    /**
     * 어떤상태일까? 초기
     */
    protected static final int STATE_NONE = 0;
    /**
     * beacon command진행중
     */
    protected static final int STATE_ING = 1;
    /**
     *  beacon command 완료
     */
    protected static final int STATE_DONE = 2;
    /**
     * beacon command 실패
     */
    protected static final int STATE_FAIL = 3;

    private boolean isInterupted = false;

    protected BaseAction() {

    }

    /**
     * 기본 생성자 추가 : 상위 class에서는 파라메터가 없는 생성자만 있음.
     * @param context 일반적인 context
     * @param macAddress beacon의 mac주소
     */
    protected BaseAction(Context context, String macAddress) {
        this.context = context;
        mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        mBluetoothManager = (BluetoothManager)context.getSystemService(Context.BLUETOOTH_SERVICE);
        this.macAddress = macAddress;
    }

    /**
     * macAddress에 있는 주소를 가진 device 객체를 가져온다.
     */
    protected void getBluetoothDevice() {
        this.mBluetoothDevice = mBluetoothAdapter.getRemoteDevice(macAddress);
    }

    /**
     * 추상화 class로 device접속을 위한 함수를 선언한다.
     */
    public abstract void connect();

    /**
     *
     */
    public void discoverServices() {

        //Logger.debug(this, "[DBG:LOST]    discoverServices Start");
        Timber.d("[DBG:LOST]    discoverServices Start");

        mBluetoothGatt.discoverServices();

        //Logger.debug(this, "[DBG:LOST]    getServices Start");
        Timber.d("[DBG:LOST]    getServices Start");

        mBluetoothGattServiceList = mBluetoothGatt.getServices();

        //Logger.debug(this, "[DBG:LOST]    getCharacteristics Start");
        Timber.d("[DBG:LOST]    getCharacteristics Start");

        for (BluetoothGattService service : mBluetoothGattServiceList) {
            mBluetoothGattCharacteristicList = service.getCharacteristics();
            //Logger.debug(this, "[DBG:LOST]    service " + service.getUuid().toString() + " is supported.");
            Timber.d("[DBG:LOST]    service " + service.getUuid().toString() + " is supported.");
        }
        //Logger.debug(this, "[DBG:LOST]    getCharacteristics End");
        Timber.d("[DBG:LOST]    getCharacteristics End");
    }


    /**
     * Beacon과의 연결을 종료한다.객체를 free
     */
    protected void close() {
        mBluetoothDevice = null;
        if(mBluetoothGatt == null) {
            return;
        }
        //mBluetoothGatt.disconnect();
        mBluetoothGatt.close();
        mBluetoothGatt = null;
    }

    /**
     * Action을 수행하는 함수.
     *
     * @param methodName 실행할 method의 명칭
     * @return result 액션이 성공했는지 실패했는지 구분할 수 있다.
     */
    protected ActionResult execute(String methodName) {
        //Logger.debug(this, "[DBG:LOST] "+ methodName +" Action  Start");
        Timber.d("[DBG:LOST] "+ methodName +" Action  Start");

        mState = STATE_NONE;

        ActionResult result = new ActionResult(SUCCESS, mState);
        int TIMEOUT = 1000 * 10;
        int timeout = 0;
        int retry = 0;
        try {
            while (!isInterupted && timeout < TIMEOUT && retry < 3 && mState != STATE_DONE) {
                //action을 수행할 때는 outOfRange에 빠지지 않도록 구현
                OutOfRangeManager.getInstance().check();

                if (mState != STATE_ING) {
                    //Logger.debug(this,  "[DBG:LOST] "+ methodName + " call before!(" + TIMEOUT + "," + retry + "," + mState + ")");

                    if(retry > 0)
                    {
                        Thread.sleep(1000);
                    }

                    mState = STATE_ING;
                    Method method = this.getClass().getMethod(methodName, null);
                    ActionResult invokeResult = (ActionResult)method.invoke(this);
                    if(invokeResult != null && invokeResult.getResult() != null)
                    {
                        result.setResult(invokeResult.getResult());
                    }

                    retry++;

                    //Logger.debug(this,  "[DBG:LOST] "+ methodName + " call end!(" + TIMEOUT + "," + retry + "," + mState + ")");
                }
                Thread.sleep(100);
                timeout += 100;
            }

            if (mState != STATE_DONE) {
                result.setStatus(FAIL);
                result.setState(mState);
            }
            else  {
                result.setStatus(SUCCESS);
                result.setState(mState);
            }

        } catch (Exception e) {
            //Logger.error(this, "[DBG:LOST] error occured!", e);
            Timber.e(e, "[DBG:LOST] error occured!");

            result.setStatus(FAIL);
            result.setState(mState);
        }

        //Logger.debug(this, " [DBG:LOST] "+ methodName + "execute End");
        Timber.d(" [DBG:LOST] "+ methodName + " execute End");

        return result;
    }

    @Override
    public void onConnectionStateChange(BluetoothGatt gatt, int status, int newState) {
        //super.onConnectionStateChange(gatt, status, newState);
        if (newState == BluetoothProfile.STATE_CONNECTED) {
            //Logger.debug(this, "[DBG:LOST]onConnectionStateChange received(CONNECTED): " + status + " -> " + newState + "( 0:disconnet, 2:connect )");
            Timber.d("[DBG:LOST]onConnectionStateChange received(CONNECTED): " + status + " -> " + newState + "( 0:disconnet, 2:connect )");
            mState = STATE_DONE;
        } else if (newState == BluetoothProfile.STATE_DISCONNECTED) {
            //Logger.debug(this, "[DBG:LOST]onConnectionStateChange received(DISCONNECTED): " + status + " -> " + newState + "( 0:disconnet, 2:connect )");
            Timber.d("[DBG:LOST]onConnectionStateChange received(DISCONNECTED): " + status + " -> " + newState + "( 0:disconnet, 2:connect )");

            mState = STATE_FAIL;
            isInterupted = true;
        }
    }

    @Override
    public void onServicesDiscovered(BluetoothGatt gatt, int status) {
        super.onServicesDiscovered(gatt, status);
        if (status == BluetoothGatt.GATT_SUCCESS) {
            //Logger.debug(this, "[DBG:LOST]onServicesDiscovered received(SUCCESS): " + status);
            Timber.d("[DBG:LOST]onServicesDiscovered received(SUCCESS): " + status);
            mState = STATE_DONE;
        } else {
            //Logger.debug(this, "[DBG:LOST]onServicesDiscovered received(NOT SUCCESS): " + status);
            Timber.d("[DBG:LOST]onServicesDiscovered received(NOT SUCCESS): " + status);
            mState = STATE_FAIL;
        }
    }

    /**
     *
     * @return isInterupted 인터럽트 상태 값을 리턴
     */
    public boolean isInterupted() {
        return isInterupted;
    }
}
